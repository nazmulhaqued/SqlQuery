Left join:
//Database name: world
//Table name: country & country_language
//column or field name: ID, Code, Name, Language, 


SELECT country.ID, Code, Name, Language 
FROM country
LEFT JOIN country_language
ON country.ID = country_language.CountryID

Left join: 
//best practic
Table name: 1) country 2) country_language

SELECT country.ID, country.Code, country.Name, country_language.Language 
FROM country
LEFT JOIN country_language
ON country.ID = country_language.CountryID


Left join:

SELECT Code, Name, Language 
FROM country
LEFT JOIN country_language
ON country.ID = country_language.CountryID


Three tables Left join:
//Tables name: country, city,cuntry_language


SELECT country.Code, country.Name, country_language.Language, city.Name, city.Population 
FROM country
LEFT JOIN country_language
ON country.ID = country_language.CountryID
LEFT JOIN city
ON country.ID = city.CountryID


Create table primary & Foreign key

CREATE TABLE users(
	id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	user_name varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	password varchar(255) NOT NULL
	 
) ENGINE=INNODB DEFAULT CHARSET=latin1;



CREATE TABLE profiles(
	id int(11) NOT NULL AUTO_INCREMENT,
	user_id int(11) NOT NULL,
	first_name varchar(255) NOT NULL,
	last_name varchar(255) NOT NULL,
	 PRIMARY KEY (id),
	 FOREIGN KEY(user_id) REFERENCES users (id)
	 ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=latin1;